#![allow(dead_code)]

use std::collections::HashMap ;
use std::fmt ;
use std::ops::{ Add, Mul, Index, IndexMut } ;

/// Factoring the type for degrees.
pub type Degree = u32 ;
/// Factoring the type for scalars.
pub type Scalar = u64 ;
fn scalar_zero() -> Scalar { 0u64 }

/**
  Enum for the variables of a polynom. Used mostly for partial derivatives.
*/
#[derive(Clone, Copy)]
pub enum Var { X, Y, Z }
impl fmt::Display for Var {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "{}",
      match * self { X => "x", Y => "y", Z => "z" }
    )
  }
}

// Bringing `X`, `Y` and `Z` in scope.
use ::Var::* ;

/**
  Degree of a monome. Indexed by `Var` for convenience.
*/
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct D3(Degree, Degree, Degree) ;
impl D3 {
  /// Creates a degree corresponding to `x^d`.
  pub fn x(d: u32) -> D3 { D3(d,0u32,0u32) }
  /// Creates a degree corresponding to `y^d`.
  pub fn y(d: u32) -> D3 { D3(0u32,d,0u32) }
  /// Creates a degree corresponding to `z^d`.
  pub fn z(d: u32) -> D3 { D3(0u32,0u32,d) }
  /// Creates a degree corresponding to `x^dx.y^dy`.
  pub fn xy(dx: u32, dy: u32) -> D3 { D3(dx,dy,0u32) }
  /// Creates a degree corresponding to `x^dx.z^dz`.
  pub fn xz(dx: u32, dz: u32) -> D3 { D3(dx,0u32,dz) }
  /// Creates a degree corresponding to `y^dy.z^dz`.
  pub fn yz(dy: u32, dz: u32) -> D3 { D3(0u32,dy,dz) }
  /// Decreases the degree of a variable. Can crash if `self[v] = 0`.
  pub fn d(& self, v: Var) -> D3 {
    let mut res = * self ;
    res[v] = res[v] - 1u32 ;
    res
  }

  /// Sets a sub-degree to zero.
  pub fn to_zero(& self, v: Var) -> D3 {
    let mut res = * self ;
    res[v] = 0u32 ;
    res
  }

  /**
    Returns the degree with the highest **sum** of sub-degrees, `self` if
    they're equal. Used to keep the highest degree (by this ordering) in
    `Poly`.
  */
  pub fn sum_max(self, rhs: D3) -> D3 {
    if self.0 + self.1 + self.2 < rhs.0 + rhs.1 + rhs.2 { rhs } else { self }
  }
}

impl Index<Var> for D3 {
  type Output = Degree ;
  fn index(& self, v: Var) -> & Degree {
    match v {
      X => & self.0, Y => & self.1, Z => & self.2
    }
  }
}
impl IndexMut<Var> for D3 {
  fn index_mut(& mut self, v: Var) -> & mut Degree {
    match v {
      X => & mut self.0, Y => & mut self.1, Z => & mut self.2
    }
  }
}
impl Add for D3 {
  type Output = D3 ;
  fn add(self, rhs: D3) -> D3 {
    D3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
  }
}
impl<'a> Add<& 'a D3> for D3 {
  type Output = D3 ;
  fn add(self, rhs: & D3) -> D3 {
    D3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
  }
}
impl<'a> Add<D3> for & 'a D3 {
  type Output = D3 ;
  fn add(self, rhs: D3) -> D3 {
    D3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
  }
}
impl<'a, 'b> Add<& 'a D3> for & 'b D3 {
  type Output = D3 ;
  fn add(self, rhs: & D3) -> D3 {
    D3(self.0 + rhs.0, self.1 + rhs.1, self.2 + rhs.2)
  }
}

/**
  A polynom. Stores a maximal degree that's not unique. For instance if
  `D3(1,2,1)` is a maximal degree then `D3(4,0,0)` can also be one. Comparison
  of degrees is by **sum** not lexical order or something else.

  Polynom itself is stored as a `HashMap<D3, Scalar>` to guarantee coefficient
  unicity. That might not be a super good idea. Whatever.

  Can be added, multiplied, and partially derived.
*/
#[derive(Clone)]
pub struct Poly {
  degree: D3,
  poly: HashMap<D3, Scalar>
}

impl fmt::Display for Poly {
  fn fmt(& self, fmt: & mut fmt::Formatter) -> fmt::Result {
    let D3(ref x, ref y, ref z) = self.degree ;
    try!(write!(fmt, "[{},{},{}] ", x, y, z)) ;
    let mut fst = true ;
    for (key, scalar) in & self.poly {
      if fst { fst = false } else { try!(write!(fmt, " + ")) } ;
      try!(write!(fmt, "{}", scalar)) ;
      if key.0 != 0u32 { try!(write!(fmt, ".x{}", key.0)) } ;
      if key.1 != 0u32 { try!(write!(fmt, ".y{}", key.1)) } ;
      if key.2 != 0u32 { try!(write!(fmt, ".z{}", key.2)) } ;
      ()
    } ;
    Ok(())
  }
}

impl Poly {
  /// Creates a polynom that's a monomial of some degree.
  pub fn of_degree(degree: D3, coef: Scalar) -> Poly {
    let mut map = HashMap::new() ;
    map.insert(degree, coef) ;
    Poly { degree: degree, poly: map }
  }
  /// Creates a polynom that's a monomial of degree `0`.
  pub fn of_scalar(coef: Scalar) -> Poly {
    Poly::of_degree(D3(0u32, 0u32, 0u32), coef)
  }

  /// The max degree of a polynom. It is not necessarily unique unless its
  /// `D3(0,0,0)`.
  pub fn max_degree(& self) -> D3 { self.degree }

  /// The partial derivative of a polynome *w.r.t.* some variable.
  pub fn d(mut self, v: Var) -> Poly {
    let mut map = HashMap::new() ;
    let mut highest = D3(0u32,0u32,0u32) ;
    for (key, scalar) in self.poly.into_iter() {
      if key[v] != 0u32 {
        let degree = key.d(v) ;
        highest = highest.sum_max(degree) ;
        map.insert(degree, scalar * (key[v] as u64)) ;
      }
    }
    if map.is_empty() { Poly::of_scalar(0u64) } else {
      self.degree = highest ;
      self.poly = map ;
      self
    }
  }

  fn internal_partial_eval<F>(& self, f: F) -> Self
  where F: Fn(& Scalar, & D3) -> (Scalar, D3) {
    let mut map = HashMap::new() ;
    let mut highest = D3(0u32,0u32,0u32) ;
    for (degree, scalar) in & self.poly {
      let (scalar, degree) = f(scalar, degree) ;
      highest = highest.sum_max(degree) ;
      map.insert(degree, scalar) ;
    }
    Poly { degree: highest, poly: map }
  }

  /// Partial evaluation of a polynom.
  pub fn partial_eval(& self, var: Var, val: Scalar) -> Self {
    let f = |s: & Scalar, d: & D3| (s * val.pow(d[var]), d.to_zero(var)) ;
    self.internal_partial_eval(f)
  }

  /// Evaluates a polynom.
  pub fn eval(& self, x: Scalar, y: Scalar, z: Scalar) -> Scalar {
    let mut res = scalar_zero() ;
    for (degree, scalar) in & self.poly {
      res = res + (
        scalar * x.pow(degree[X]) * y.pow(degree[Y]) * z.pow(degree[Z])
      )
    } ;
    res
  }
}

impl Add for Poly {
  type Output = Poly ;
  fn add(mut self, mut rhs: Poly) -> Poly {
    // Update everything in `self`, removing from `rhs`.
    for (key, scalar) in self.poly.iter_mut() {
      if let Some(other) = rhs.poly.get(key) {
        * scalar = *scalar + other ;
      }
      rhs.poly.remove(key) ;
    } ;
    // Drain `rhs` to add to `self`.
    for (key, scalar) in rhs.poly.into_iter() {
      self.poly.insert(key, scalar) ;
    } ;
    // Update `degree`.
    self.degree = self.degree.sum_max(rhs.degree) ;
    self
  }
}

impl Mul for Poly {
  type Output = Poly ;
  fn mul(self, rhs: Poly) -> Poly {
    let mut map = HashMap::new() ;
    // Update everything in `self`, removing from `rhs`.
    for (l_key, l_scalar) in self.poly.into_iter() {
      for (r_key, r_scalar) in rhs.poly.iter() {
        map.insert( l_key + r_key, l_scalar * r_scalar ) ;
      }
    } ;
    // Build new poly.
    Poly{ degree: self.degree + rhs.degree, poly: map }
  }
}

fn plus(lhs: & Poly, rhs: & Poly) -> Poly {
  println!("  {}", lhs) ;
  println!("+ {}", rhs) ;
  let res = lhs.clone() + rhs.clone() ;
  println!("= {}\n", res) ;
  res
}

fn times(lhs: & Poly, rhs: & Poly) -> Poly {
  println!("  {}", lhs) ;
  println!("* {}", rhs) ;
  let res = lhs.clone() * rhs.clone() ;
  println!("= {}\n", res) ;
  res
}

fn derive(poly: & Poly, v: Var) -> Poly {
  println!("d {}", poly) ;
  println!("/ d{}", v) ;
  let res = poly.clone().d(v) ;
  println!("= {}\n", res) ;
  res
}

fn eval(poly: & Poly, x: Scalar, y: Scalar, z: Scalar) {
  println!("  {}", poly) ;
  println!("|> x = {}, y = {}, z = {}", x, y, z) ;
  let res = poly.eval(x,y,z) ;
  println!("= {}\n", res)
}

fn peval(poly: & Poly, var: Var, val: Scalar) -> Poly {
  println!("  {}", poly) ;
  println!("|> {} = {}", var, val) ;
  let res = poly.partial_eval(var, val) ;
  println!("= {}\n", res) ;
  res
}

fn main() {
  println!("\n\n|==============================|\n") ;
  let ref p1 = Poly::of_scalar(42u64) ;
  let ref p2 = Poly::of_degree(D3::x(3u32), 7u64) ;
  let ref p3 = Poly::of_degree(D3::xy(7u32,2u32), 9u64) ;
  let ref p4 = Poly::of_degree(D3(1u32,3u32,9u32), 16u64) ;
  let ref p5 = plus(p1, p2) ;
  let ref p6 = times(p5, p3) ;
  let ref p7 = plus(p6, p4) ;
  let _ = derive(p7, X) ;
  let _ = derive(p7, Y) ;
  let _ = derive(p7, Z) ;
  eval(p4, 2u64, 3u64, 5u64) ;
  peval(p6, Y, 7u64) ;
  println!("|==============================|\n\n") ;
}
